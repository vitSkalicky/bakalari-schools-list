#!/usr/bin/python
# This script pull data about schools from Bakalari's API and puts them into one convinient file.
import urllib.request
import urllib.error
import urllib.parse
import xml.etree.ElementTree as ET
import sys
import json
import getopt

argv = sys.argv[1:]

opts, args = getopt.getopt(argv,"")

if len(args)!=1:
    raise SystemExit(f"Usage: {sys.argv[0]} <output file>")

apiURL = "https://sluzby.bakalari.cz/api/v1/municipality/"

class School:
    name: str
    url: str
    id: str

schools = list()

for i in ["a","á","b","c","č","d","ď","e","é","ě","f","g","h","ch","i","í","j","k","l","m","n","ň","o","ó","p","q","r","ř","s","š","t","ť","u","ú","ů","v","w","x","y","ý","z","ž"]:
    try:
        contents = urllib.request.urlopen(apiURL + urllib.parse.quote_plus(str(i))).read()
        root = ET.fromstring(contents)

        schoolsT = root.find("schools")
        for child in schoolsT:
            school = School()
            school.name = child.find("name").text
            school.url = child.find("schoolUrl").text
            school.id = child.find("id").text
            schools.append(school)
    except urllib.error.HTTPError as e:
        if e.code != 404:
            print(f"Could not fetch school list for query {i}: {e}", file=sys.stderr)

if len(schools) == 0:
    raise SystemExit(f"Failed to fetch any schools")

# add test server (https://gitlab.com/vitSkalicky/lepis-rozvrh-test-server) to the list.
testServer = School()
testServer.name = "Testovací server pro Lepší rozvrh"
testServer.url = "https://lepsi-rozvrh-testserver.vit-skalicky.cz"
testServer.id = "LRTESTSERV"

schools.append(testServer)

jsonString = json.dumps(schools, default=lambda o: o.__dict__, indent=2, ensure_ascii=False)

f = open(args[0], "w")
f.write(jsonString)
f.close
