# Bakalari schools list

Simple script that fetches list of all schools and their URLs from the Bakaláři API (https://sluzby.bakalari.cz/api/v1/municipality/) and combines it into one json. The Bakaláři API a little bit stupid and requieres you to ask for the list of towns and then you need a seperate request for every town to get all the schools with their URLs.

This script is run every Friday at 22:00 and you can find the list of all schools in Json format on https://vitskalicky.gitlab.io/bakalari-schools-list/schoolsList.json
